/* eslint-disable import/prefer-default-export */
import { TOKEN_TYPES as CE_TOKEN_TYPES } from '~/filtered_search/constants';

export const TOKEN_TYPES = [...CE_TOKEN_TYPES, 'approver'];
